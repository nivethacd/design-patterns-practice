import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage {
    WebDriver driver;

    public HomePage(WebDriver driver) {
        this.driver = driver;
    }

    By acceptCookies = By.cssSelector("button[id=onetrust-accept-btn-handler]");
    By searchFrom = By.id("from.search");
    By searchTo = By.id("to.search");
    By twoWay = By.id("return");
    By nextDay = By.xpath("//*[@id=\"app\"]/div/div[1]/main/div[2]/div[4]/div/div/div[1]/section/form/div[3]/fieldset[1]/div[1]/button[2]");
    By returnNextDay = By.id("page.journeySearchForm.inbound.title");
    By searchTickets = By.cssSelector("button[data-test='submit-journey-search-button']");


    public void searchForm(String fromStation) {
        driver.findElement(searchFrom).sendKeys(fromStation);
    }

    public void toStation(String toStation) {
        driver.findElement(searchTo).sendKeys(toStation);
    }

    public void twoWayJourney(){
        WebElement radioBtn1 = driver.findElement(By.id("return"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click()", radioBtn1);
    }

    public void nextDayJourney(){
       driver.findElement(nextDay).click();
    }

    public void returnNextDay(){
        driver.findElement(returnNextDay).sendKeys("20-Jul-21");
    }

    public void searchTicket(){
        WebElement subbtn = driver.findElement(By.cssSelector("button[data-test='submit-journey-search-button']"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click()", subbtn);
    }
}
