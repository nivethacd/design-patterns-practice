import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

//Home page
public class SearchTrainPage extends Initialisation {

    @Test
    public void searchTicketTest() {

        WebDriver driver = getDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        //HomePage
        HomePage homePage = new HomePage(driver);
        driver.findElement(homePage.acceptCookies).click();
        homePage.searchForm("Gatwick Airport");
        homePage.toStation("Leeds");
        homePage.twoWayJourney();
        homePage.nextDayJourney();
        homePage.returnNextDay();
        homePage.searchTicket();

        //TrainListPage
        TrainListPage trainListPage = new TrainListPage(driver);
        System.out.println(trainListPage.getTotalPrice());
    }
}
