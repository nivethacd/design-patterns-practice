import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class TrainListPage {
    WebDriver driver;

    public TrainListPage(WebDriver driver) {
        this.driver = driver;
    }

    By byTotalPrice = By.xpath("//*[@data-test='cjs-price']"); //total

    public String getTotalPrice() {
        return driver.findElement(byTotalPrice).getText();
    }

}
